# Electron Svelte starter tempate

This is the code for the tutorial [Getting started with Svelte and Electron](https://home.aveek.io/blog/post/electron-svelte/)

## Run for development

```
npm run app-dev
```